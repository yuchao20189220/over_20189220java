import java.io.*;
public class MyOD{
    public static void main(String[] args) throws IOException {
        try (FileInputStream input = new FileInputStream("/home/rocedu/hello")) {
            byte[] data = new byte[1024];
            int i, flag;
            input.read(data);
            for (i = 0; i < 1024; i = i + 4) {
                if (i % 16 == 0) {
                    System.out.printf("\n%07o\t\t", i );
                }  //四个字节为一组，一行四组。i=16时为左侧第一列(默认的地址），格式为七位八进制。
                // 通过找规律，其数值是该行第一个字符的序号值（从0到length-1）对应的的八进制数
                System.out.printf(" %02x%02x%02x%02x\t", data[i + 3], data[i + 2], data[i + 1], data[i]);
                if ((i + 4) % 16 == 0) {
                    System.out.println();
                    System.out.printf("\t  ");
                    for (int j = i - 12; j < i+4 ; j++) {
                        if ((int) data[j] == 10) {
                            System.out.printf(" \\");
                            System.out.printf("n ");
                        } else {
                            System.out.printf("  %c ", data[j]);
                        }
                    }
                }
                if (data[i+4] ==0 ) {
                    System.out.println();
                    System.out.printf("\t   ");
                    for (int j = i-i%16; data[j-3] != 0; j++) {
                        if ((int) data[j] == 10) {
                            System.out.printf(" \\");
                            System.out.printf("n  ");
                        } else {
                            System.out.printf(" %c ", data[j]);
                        }
                    }
                    break;
                }
            }
            System.out.printf("\n%07o\n", i+3 );
        }
    }
}