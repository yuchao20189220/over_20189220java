// Server Classes
abstract class Data {
    abstract public void DisplayValue();
}
class Byte extends  Data {
    int value;
    Byte() {
        value=100;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
// Pattern Classes
abstract class Factory {
    abstract public Data CreateDataObject();
}
class ByteFactory extends Factory {
    public Data CreateDataObject(){
        return new Byte();
    }
}
//Client classes
class Document {
    Data pd;
    Document(Factory pf){
        pd = pf.CreateDataObject();
    }
    public void DisplayData(){
        pd.DisplayValue();
    }
}
//Test class
public class MyDoc {
    static Document d;

    public static void main(String[] args) {
        d = new Document(new ByteFactory());
        d.DisplayData();
    }
}
