﻿public class PC extends Object{
//aaa
    CPU cpu;
    HardDisk HD;
    PC() {}
    PC(CPU cpu) {
	this.cpu = cpu;
    }
    PC(HardDisk HD) {
	this.HD = HD;
    }
    PC(CPU cpu,HardDisk HD) {
	this.cpu = cpu;
	this.HD = HD;
    }
    public void setCPU(CPU a) {
	cpu = a;
    }
    public void setHardDisk(HardDisk b) {
	HD = b;
    }
    public void show() {
	System.out.printf("CPU的速度为: %d \n",cpu.getSpeed());
	System.out.printf("磁盘容量为: %d \n",HD.getAmount());
    }
    public String toString() {
	return "PC已覆盖toString";
    }
    public boolean equals(Object obj) {
	return true;
    }
}
