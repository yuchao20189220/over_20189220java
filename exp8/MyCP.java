import java.io.*;
public class MyCP {
    public static void main(String args[]) {
        String choose=args[0];
        //获得第一个参数
        String File1=args[1];
        //获得第二个参数：文件名
        String File2=args[2];
        //获得第三个参数：文件名
        File sourceFile = new File(File1);
        //读取的文件
        File targetFile = new File(File2);
        //写入的文件
        int ReadLenth=0;
        if (choose.equals("-tx")){
            ReadLenth=1;
        }
        //如果参数是-tx，每次读取的长度设置为1
        else if(choose.equals("-xt")) {
            ReadLenth=4;
        }
        //如果参数是-xt，每次读取的长度设置为4
        else{
            System.out.println("输入参数错误！");
            System.exit(0);
        }
        //如果参数输入错误的话就退出程序

        char c[] =new char[ReadLenth];
        //定义字符数组
        try{
            Writer out = new FileWriter("D:\\hhhhhhhhhhhh\\exp8\\src\\main\\java\\a.text");
            //指向目的地的输出流
            Reader in  = new FileReader("D:\\hhhhhhhhhhhh\\exp8\\src\\main\\java\\b.bin");
            //指向源的输入流
            int n = -1;
            while((n=in.read(c,0,ReadLenth))!=-1) {
                //当文件中还有信息时，继续读取字符到字符数组c中，n是返回的实际读取的字符的个数
                String number="";
                if(ReadLenth==1){
                    number=Integer.toBinaryString((int)c[0]-48);
                    //把读取到的十进制数转化为二进制数
                    while(number.length()<4){
                        number="0"+number;
                        //把二进制数扩充为4位
                    }
                    out.write(number);
                    //写入目标文件
                }
                else if(ReadLenth==4){
                    for(int i=0;i<n;i++){
                        number=number+c[i];
                        //读取4位为二进制数
                    }
                    number=Integer.valueOf(number,2).toString();
                    //二进制数转化十进制，再转为字符串形式
                    out.write(number);
                    //写入目标文件
                }
            }
            out.flush();
            out.close();
        }
        catch(IOException e) {
            System.out.println("Error "+e);
        }
    }
}
