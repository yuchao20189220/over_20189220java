import static org.junit.Assert.*;
import org.junit.Test;
import junit.framework.TestCase;
/**
 * Created by yuchao20189220
 */
public class ComplexTest extends TestCase {
    Complex complex = new Complex(1,1);
    @Test
    public void testAdd(){
        assertEquals(new Complex(3.3,3.4), complex.ComplexAdd(new Complex(2.3,2.4)));
        assertEquals(new Complex(0,5.5), complex.ComplexAdd(new Complex(2.23,2.24)));
    }
    //测试加法
    @Test
    public void testSub(){
        assertEquals(new Complex(-5.3,-2.4), complex.ComplexSub(new Complex(6.3,3.4)));
        assertEquals(new Complex(5.3,-2.4), complex.ComplexSub(new Complex(-6.3,3.4)));
    }
    //测试减法
    @Test
    public void testMulti(){
        assertEquals(new Complex(3.0,2.0), complex.ComplexMulti(new Complex(3.0,2.0)));
        assertEquals(new Complex(1.0,1.0), complex.ComplexMulti(new Complex(8.0,2.0)));
    }
    //测试乘法
    @Test
    public void testDiv(){
        assertEquals(new Complex(1.0,1.0), complex.ComplexDiv(new Complex(1.0,1.0)));
        assertEquals(new Complex(0.0,0.0), complex.ComplexDiv(new Complex(1.0,0.0)));
        //assertEquals(new Complex(0.0,0.0), complex.ComplexDiv(new Complex(3,4)));
        //边缘测试
    }
    @Test
    public void testequals(){
        assertEquals(true, complex.equals(new Complex(1.0,1.0)));
    }
    //测试判断相等
}

