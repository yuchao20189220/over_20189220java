import java.util.*;
class UDiscKey implements Comparable {
    double key=0;
    UDiscKey(double d) {
        key=d;
    }
    public int compareTo(Object b) {
        UDiscKey disc=(UDiscKey)b;
        if((this.key-disc.key)==0)
            return -1;
        else
            return (int)((this.key-disc.key)*1000);
    }
}
class UDisc{
    int amount;
    double price;
    UDisc(int m,double e) {
        amount=m;
        price=e;
    }
}
public class 15_3 {
    public static void main(String args[ ]) {
        TreeMap<UDiscKey,UDisc>  treemap= new TreeMap<UDiscKey,UDisc>();
        int amount[]={1,2,4,8,16};
        double price[]={867,266,390,556};
        UDisc UDisc[]=new UDisc[4];
        for(int k=0;k<UDisc.length;k++) {
            UDisc[k]=new UDisc(amount[k],price[k]);
        }
        UDiscKey key[]=new UDiscKey[4] ;
        for(int k=0;k<key.length;k++) {
            key[k]=new UDiscKey(UDisc[k].amount);//按容量排列
        }
        for(int k=0;k<UDisc.length;k++) {
            treemap.put(key[k],UDisc[k]);
        }
        int number=treemap.size();
        System.out.println("树映射中有"+number+"个对象,按容量排序:");
        Collection<UDisc> collection=treemap.values();
        Iterator<UDisc> iter=collection.iterator();
        while(iter.hasNext()) {
            UDisc disc=iter.next();
            System.out.println(""+disc.amount+"G "+disc.price+"元");
        }
        treemap.clear();
        for(int k=0;k<key.length;k++) {
            key[k]=new UDiscKey(UDisc[k].price);//按价格排列
        }
        for(int k=0;k<UDisc.length;k++) {
            treemap.put(key[k],UDisc[k]);
        }
        number=treemap.size();
        System.out.println("树映射中有"+number+"个对象,按价格排序:");
        collection=treemap.values();
        iter=collection.iterator();
        while(iter.hasNext()) {
            UDisc disc=iter.next();
            System.out.println(""+disc.amount+"G "+disc.price+"元");
        }
    }
}
