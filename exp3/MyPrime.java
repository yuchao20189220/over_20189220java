﻿import java.util.Scanner;
/**
 *
 * @author yuchao20189220
 */
public class MyPrime {
    public static void main(String[] args) {
        System.out.println("please input n:");
        Scanner scanner = new Scanner(System.in);
        int n = getN(scanner);
        int i1 = 32767;
	
        if(n<1||n> i1){
            System.out.println("Please check your input!");
            System.exit(0);
        }
        System.out.println("1到"+ n +" 中是质数的值有：");
        for(int i=2;i<=n;i++){
            int flag = 0;
            for(int j=2;j<i;j++) {
                if (i % j == 0)
                    flag = 1;
            }
            if(flag==0){
                System.out.println(i);
            }
        }
    }

    private static int getN(Scanner scanner) {
        return scanner.nextInt();
    }
}
