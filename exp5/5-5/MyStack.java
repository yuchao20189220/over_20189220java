public class MyStack<T> implements Stack<T> {
    private Object element[];
    private int top;
    public MyStack(int size){
        this.element = new Object[Math.abs(size)];
        this.top = -1;
    }
    public MyStack() {
        this(64);
    }
    public boolean isEmpty() {
        return this.top == -1;
    }
    public void push(T x) {
        if(x==null)
            return;
        if(this.top == element.length-1){
            Object[] temp = this.element;
            this.element = new Object[temp.length*2];
            for(int i = 0; i < temp.length; i++)
                this.element[i] = temp[i];
        }
        this.top++;
        this.element[this.top] = x;
    }
    public T pop() {
        return this.top==-1 ? null:(T)this.element[this.top--];
    }
    public T get() {
        return this.top==-1 ? null:(T)this.element[this.top];
    }
}

